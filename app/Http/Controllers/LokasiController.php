<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lokasi;
use RealRashid\SweetAlert\Facades\Alert;

class LokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lokasi= Lokasi::all();
        return view('lokasi.index',compact('lokasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lokasi=lokasi::all();
        // dd($lokasi);
        return view('lokasi.create',compact('lokasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nama'=>'required',
        ]);

        $lokasi = Lokasi::create([
            'nama'  => $request->nama,
        ]);
        Alert::success('Success', 'Berhasil Membuat Lokasi');
        return redirect ('/lokasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lokasi=Lokasi::all();
        $lokasi=Lokasi::find($id);
        return view ('lokasi.show',compact('lokasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lokasi=Lokasi::all();
        $lokasi=Lokasi::find($id);
        // dd($lokasi);
        return view('lokasi.edit',compact('lokasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
        ]);

        $lokasi = lokasi::find($id);
        $lokasi->nama = $request->nama;
        $lokasi->update();
        Alert::success('Success', 'Berhasil Memperbarui Lokasi');
        return redirect ('/lokasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lokasi=Lokasi::all();
        $lokasi=Lokasi::find($id);
        $lokasi->delete();
        Alert::success('Success', 'Berhasil Menghapus Lokasi');
        return redirect ('/lokasi');
    }
}
