<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\profile;
use File;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Cache;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRestoranServerSide()
    {
        $data = Restoran::latest()->get();
        return Datatables::of($data)
            ->editColumn("created_at", function ($data) {
                return date("m/d/Y", strtotime($data->created_at));
            })
            ->addColumn('ID', function ($data) {
                $update = '<a href="javascript:void(0)" class="btn btn-primary">' . $data->id . '</a>';
                return $update;
            })
            ->rawColumns(['ID'])
            ->make(true);
    }

    public function indexGetRestoran()
    {
        return view("restoran_server_side");
    }

    public function index()
    {
        $userlogin = auth()->user(); //mengetahui user login
        $user_id = $userlogin->id;

        $lastprofile = profile::where('user_id', $user_id)->first(); //memanggil profile user yang login
        if ($lastprofile != null ) {
            return view('profile.index', compact('lastprofile'));
        } else {
            return view('profile.create');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'notelp' =>'required',
            'bio'=>'required',
            'poto'=>'mimes:jpg,png,jpeg|max:2000'
        ]);
        $foto = $request->poto;
        $new_foto = time() . ' - ' . $foto->getClientOriginalName();
        $userlogin = auth()->user();
        // dd($userlogin->id);
        $user_id = $userlogin->id;
        $profile = profile::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'notelp' => $request->notelp,
            'bio' => $request->bio, 
            'poto'=> $new_foto,
            'user_id'=> $user_id
        ]);
        $foto->move('uploads/profile/', $new_foto);
        Alert::success('Success', 'Berhasil Membuat Profile');

        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = profile::find($id);
        // dd($profile);
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'notelp' =>'required',
            'bio'=>'required',
            'poto'=>'mimes:jpg,png,jpeg|max:2000'
        ]);

        $profile = profile::findorfail($id);

        if($request->has('poto')){
            $path="uploads/profile/";
            File::delete($path.$profile->poto);
            $foto= $request->poto;
            $new_foto = time() . ' - ' . $foto->getClientOriginalName();
            $foto->move('uploads/profile' , $new_foto);
            $data_profile = [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'notelp' => $request->notelp,
                'bio' => $request->bio, 
                'poto'=> $new_foto
            ];
        }else{
            $data_profile = [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'notelp' => $request->notelp,
                'bio' => $request->bio, 
            ];
        }
        $profile->update($data_profile);
        Alert::success('Success', 'Berhasil Mengubah Profile');
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
