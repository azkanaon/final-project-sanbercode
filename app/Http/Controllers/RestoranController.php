<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Restoran;
use App\Lokasi;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class RestoranController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('show','home');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $restoran= Restoran::all();
        return view('restoran.home',compact('restoran'));
    }
    
    
     public function index()
    {
        $restoran= Restoran::all();
        return view('restoran.index',compact('restoran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lokasi=lokasi::all();
        // dd($lokasi);
        return view('restoran.create',compact('lokasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama'=>'required',
            'alamat'=>'required',
            'jam_buka'=>'required',
            'kontak'=>'required',
            'range_harga'=>'required',
            'lokasi_id'=>'required',
            'foto'=>'required|mimes:jpeg,png,jpg|max:2200'
        ]);
       

        $foto = $request->foto;
        $new_foto = time() . '_' . $foto->getClientOriginalName();

        $restoran = Restoran::create([
            'nama'  => $request->nama,
            'lokasi_id'  => $request->lokasi_id,        
            'alamat'  => $request->alamat,
            'jam_buka'  => $request->jam_buka,
            'kontak'  => $request->kontak,
            'range_harga'  => $request->range_harga,
            'foto' => $new_foto
        ]);

        $foto->move('img/', $new_foto);
        Alert::success('Success', 'Berhasil Menambahkan Restoran');
        return redirect ('/restoran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restoran=restoran::all();
        $restoran=Restoran::find($id);
        // dd($restoran);
        return view ('restoran.show',compact('restoran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restoran=restoran::all();
        $lokasi=lokasi::all();
        $restoran=Restoran::find($id);
        // dd($restoran);
        return view('restoran.edit',compact('lokasi','restoran'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'alamat'=>'required',
            'jam_buka'=>'required',
            'kontak'=>'required',
            'range_harga'=>'required',
            'lokasi_id'=>'required',
            'foto'=>'mimes:jpeg,png,jpg|max:2200'
        ]);
       
        $restoran = Restoran::find($id);

        if($request->has('foto')){
            $path="img/";
            File::delete($path.$restoran->foto);
            $foto = $request->foto;
            $new_foto = time() . '_' . $foto->getClientOriginalName();
            $foto->move('img/', $new_foto);
            
            $data = [
            'nama'  => $request->nama,
            'lokasi_id'  => $request->lokasi_id,        
            'alamat'  => $request->alamat,
            'jam_buka'  => $request->jam_buka,
            'kontak'  => $request->kontak,
            'range_harga'  => $request->range_harga,
            'foto' => $new_foto];
        } else {
            $data = [
            'nama'  => $request->nama,
            'lokasi_id'  => $request->lokasi_id,        
            'alamat'  => $request->alamat,
            'jam_buka'  => $request->jam_buka,
            'kontak'  => $request->kontak,
            'range_harga'  => $request->range_harga,
            ];
        }
        $restoran->update($data);
        Alert::success('Success', 'Berhasil Memperbarui Restoran');

        return redirect ('/restoran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restoran=restoran::all();
        $restoran=Restoran::find($id);
        $restoran->delete();
        Alert::success('Success', 'Berhasil Menghapus Restoran');
        return redirect ('/restoran');
    }
}
