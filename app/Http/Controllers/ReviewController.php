<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Review;
use App\Restoran;
use App\User;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review= Review::all();
        return view('review.index',compact('review'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $review=Review::all();
    //     var_dump($review->users);
    //    dd($review);
        return view('review.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'=>'required',
            'review'=>'required',
            'nilai' =>'required',
            'poto'=>'mimes:jpg,png,jpeg|max:2000'

        ]);
        $foto = $request->poto;
        $new_foto = time() . ' - ' . $foto->getClientOriginalName();
        $review = Review::create([
    		'judul' => $request->judul,
    		'review' => $request->review,
            'nilai' => $request->nilai,
            'poto'=> $new_foto
        ]);
        $foto->move('img/', $new_foto);
        
        Alert::success('Success', 'Berhasil Menambahkan Review');
        return redirect('/review');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review=Review::all();
        //$restoran=restoran::all();
        $review=Review::find($id);
        // dd($review);
        return view('review.edit',compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul'=>'required',
            'review'=>'required',
            'nilai' =>'required',
            'poto'=>'mimes:jpg,png,jpeg|max:2000'
        ]);

        $review = review::findorfail($id);

        if($request->has('poto')){
            $path="img/";
            File::delete($path.$review->poto);
            $foto= $request->poto;
            $new_foto = time() . ' - ' . $foto->getClientOriginalName();
            $foto->move('img' , $new_foto);
            $data = [
                'judul' => $request->judul,
                'review' => $request->review,
                'nilai' => $request->nilai,
                'poto'=> $new_foto
            ];
        }else{
            $data = [
                'judul' => $request->judul,
                'review' => $request->review,
                'nilai' => $request->nilai,
            ];
        }
        $review->update($data);
        Alert::success('Success', 'Berhasil Memperbarui Review');
        return redirect('/review');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review=Review::all();
        $review=Review::find($id);
        $review->delete();
        Alert::success('Success', 'Berhasil Menghapus Review');
        return redirect ('/review');
    }
}
