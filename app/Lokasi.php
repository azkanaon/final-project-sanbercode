<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = 'lokasi';
    protected $fillable = ['nama'];

    public function restoran(){
        return $this->hasMany(Restoran::class);
    }

    public $timestamps = false;
}
