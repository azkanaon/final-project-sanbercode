<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restoran extends Model
{
    protected $table = 'restoran';
    protected $fillable = ['foto','nama','alamat','jam_buka','kontak','range_harga','lokasi_id'];

    public function lokasi(){
        return $this->belongsTo(Lokasi::class);
    }

    public function review(){
        return $this->hasMany(Review::class);
    }

    public $timestamps = false;

}
