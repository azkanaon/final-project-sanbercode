<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $fillable = ['judul','review','nilai','user_id','restoran_id','poto'];

    public function restoran() {
        return $this->belongToMany(App\Restoran);
    }

    public function users() {
        return $this->belongsToMany(App\User);
    }
}
