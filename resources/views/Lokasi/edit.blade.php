@extends('layouts.master')

@section('content')
    <h3 class="card-title">Buat Entri Lokasi Baru</h3>
      <form action="/lokasi/{{$lokasi->id}}" method="POST">
         @csrf
         @method('PUT')
         <div class="form-group">
           <label for="nama"><b>Nama Lokasi<b></label>
           <input type="text" class="form-control" name="nama" id="nama" value="{{old('nama',$lokasi->nama )}}" placeholder="Masukkan Nama Lokasi"
            @error('nama')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <button type="submit" class="btn btn-primary mt-3">Edit</button>
        </form>

@endsection