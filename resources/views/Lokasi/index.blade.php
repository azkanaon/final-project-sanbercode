@extends('layouts.master')

@section('content')
<div >


</div>
<div class="card mx-auto" style="width:75%">
              <div class="card-header">
                <h3 class="card-title">List Lokasi</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a class ="btn btn-primary mb-3" href="/lokasi/create"> Create New lokasi </a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px; text-align:center;">#</th>
                      <th style="width: 80%; text-align:center;">Nama</th>
                      <th style="width:1px; text-align:center;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                   @forelse ($lokasi as $key => $lokasi)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td style="text-align:center; text-transform:capitalize;"> {{ $lokasi -> nama }} </td>
                        <td style="display :flex;"> 
                            <a class="btn btn-info" href="/lokasi/{{ $lokasi->id}}"> Show </a>
                            <a class="btn btn-secondary" href="/lokasi/{{ $lokasi->id }}/edit"> Edit </a> 
                            <form action="/lokasi/{{$lokasi->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center"> lokasi tidak tersedia </td>
                    </tr?
                    @endforelse
                  </tbody>
                </table>
              </div>

              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection