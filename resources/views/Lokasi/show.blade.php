@extends('layouts.master')

@section('content')
    <button type="button" disabled class="btn btn-outline-light" style="background-color: #F24B27; margin: 0 auto;">
        <h1> LOKASI YANG ANDA PILIH </h1>
        <a style="font-size:70px"> -{{$lokasi->nama}}-</a>
    </button>
@endsection