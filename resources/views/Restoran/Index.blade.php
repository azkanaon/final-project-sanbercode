@extends('layouts.master')

@section('content')
<div>


</div>
<div class="card" style="width:95%">
              <div class="card-header">
                <h3 class="card-title">List Restoran</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a class ="btn btn-primary mb-3" href="/restoran/create"> Create New restoran </a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px; text-align:center;">#</th>
                      <th style="width: 150px; text-align:center;">Nama</th>
                      <th style="width: 30px; text-align:center;">Lokasi</th>
                      <th style="text-align:center;">Alamat</th>
                      <th style="text-align:center;">Jam Buka</th>
                      <th style="text-align:center;">No Telepon</th>
                      <th style="text-align:center;">Range Harga</th>
                      <th style="width: 20px; text-align:center; ">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                   @forelse ($restoran as $key => $restoran)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $restoran -> nama }} </td>
                        <td> {{ $restoran -> lokasi->nama }} </td>
                        <td> {{ $restoran ->alamat }} </td>
                        <td> {{ $restoran ->jam_buka }} </td>
                        <td> {{ $restoran ->kontak }} </td>
                        <td> {{ $restoran ->range_harga }} </td>
                        <td style="display :flex;"> 
                            <a class="btn btn-info" href="/restoran/{{ $restoran->id}}"> Show </a>
                            <a class="btn btn-secondary" href="/restoran/{{ $restoran->id }}/edit"> Edit </a> 
                            <form action="/restoran/{{$restoran->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8" align="center"> Restoran Tidak Tersedia </td>
                    </tr?
                    @endforelse
                  </tbody>
                </table>
              </div>

              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection