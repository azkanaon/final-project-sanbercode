@extends('layouts.master')

@section('content')
    <h3 class="card-title">Edit Entri Restoran Baru</h3>
      <form action="/restoran/{{$restoran->id}}" method="POST" enctype="multipart/form-data">
         @csrf
         @method('PUT')
         <div class="form-group">
           <label for="nama"><b>Nama Restoran<b></label>
           <input type="text" class="form-control" name="nama" id="nama" value="{{old('nama',$restoran->nama)}}" placeholder="Masukkan nama restoran"
            @error('nama')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        
        
        <div class="form-group">
            <label for="alamat">Alamat Restoran</label>
           <textarea class="form-control" name="alamat" id="alamat"  placeholder="Masukkan alamat restoran" cols="30" rows="3"> 
           {{old('alamat',$restoran->alamat )}}
           </textarea>
            @error('alamat')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="lokasi_id">Lokasi &nbsp :&nbsp </label>
            <select name="lokasi_id" id="lokasi_id">
            <option value="{{old('judul',$restoran->lokasi->id )}}"">{{old('item->nama',$restoran->lokasi->nama )}}</option>
            @foreach($lokasi as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
            </select>
            @error('lokasi_id')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>  

        <div class="form-group">
            <label for="jam_buka">Jam Buka Restoran</label>
           <input type="text" class="form-control" name="jam_buka" value="{{old('jam_buka',$restoran->jam_buka )}}" id="jam_buka" placeholder="Masukkan jam_buka restoran"
            @error('jam_buka')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="kontak">Kontak Restoran</label>
           <input type="number" class="form-control" name="kontak" value="{{old('kontak',$restoran->kontak )}}" id="kontak" placeholder="Masukkan kontak restoran"
            @error('kontak')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group"> 
            <label for="range_harga">Range Harga Menu &nbsp : &nbsp</label>
            <select name="range_harga" id="range_harga">
                <option value="{{old('range_harga', $restoran->range_harga )}}"">{{old('range_harga',$restoran->range_harga  )}}</option>
                <option value="Dibawah 50.000 per orang">Dibawah 50.000 per orang</option>
                <option value="50.000 - 100.000 per orang">50.000 - 100.000 per orang</option>
                <option value="100.000-200.000 per orang">100.000-200.000 per orang</option>
                <option value="Diatas 200.000 per orang">Diatas 200.000 per orang</option>
            </select>
            @error('range_harga')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="foto"> Foto </label>
            <input type="file" class="form-control" name="foto" id="foto">
            @error('foto')
                 <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

            <button type="submit" class="btn btn-primary">Edit</button>
        </form>

@endsection