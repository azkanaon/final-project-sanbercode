@extends('layouts.master')

@section('content')
<div class="m-2">
    <div class="container" style="mt-5">
    <div class="d-flex flex-column">
        <button class="btn btn-outline-light mt-2 mb-5">
            <a class="my-5" style="color:#F24B27;font-family:Tahoma; font-weight:1000; font-size:30px; text-align:center; " href="/" > MAKANN ENAK </a>  
        </button>
    </div>
    <a class ="btn btn-primary mb-3" href="/restoran"> Tambah Restoran </a>
    <a class ="btn btn-primary mb-3" href="/lokasi"> Tambah Lokasi </a>
    <div class="d-flex flex-column">
        <div class="row" style="mt-5">
        @foreach($restoran as $key => $restoran)
                <div class="col-md-3"  >
                <div class="card" style="width: 20rem ml-5">
                        <img class="card-img-top" src=" {{ asset('img/' . $restoran->foto) }} " alt="Card image cap" width="100px" height="160px" background-size="cover">
                <div class="card-body">
                    <h4 class="card-title, ml-2"">{{$restoran->nama}}</h4>
                    <p class="card-text" >{{Str::limit($restoran->alamat,75) }}</p>
                    <a href="/restoran/{{$restoran->id}}" class="btn btn-primary">Detail</a>
                </div>
                </div>
                </div>
            @endforeach
    </div>
@endsection