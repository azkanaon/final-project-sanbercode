@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col md-6">
      <button type="button" class="btn btn-danger btn-sm">{{ $restoran->lokasi->nama }}</button>
      <div class="ratio ratio-4x3">
        <img class="card-img-top mt-2" src="{{ asset('img/' . $restoran->foto) }}" alt="Card image cap" width="250">
      </div>
    </div>
    <div class="col md-6">
      <div class="d-flex flex-column">
        <button type="button" class="btn btn-outline-light" style="background-color: #F24B27;">
          <h1>{{$restoran->nama}}  <br></h1>
        </button>
      </div>
    <p class="mt-3" style="font-size:24px;"> Alamat &nbsp &nbsp  : {{ $restoran->alamat }} <br> </p>
    <p style="font-size:24px;"> Jam Buka :  {{ $restoran->jam_buka }} </p>
    <p style="font-size:24px;"> 
    <span>
     No Telp &nbsp &nbsp: 
    </span>
    <span>
    <a href="tel:{{$restoran->kontak}}">{{ $restoran->kontak }}</a> 
    </span></p>
    <p class="mb-5" style="font-size:24px;"> Harga &nbsp &nbsp &nbsp: {{ $restoran->range_harga }}  </p>

    <a class="btn btn-info" href="/review"> REVIEW </a>

    
    </div>
  </div>
@endsection