<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="{{asset('img/gambar3.png')}}" rel="icon">
  <title>Review Makanan/Minuman Indonesia</title>
  <link href="{{asset('ruang-admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('ruang-admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('ruang-admin/css/ruang-admin.min.css" rel="stylesheet')}}">
</head>

<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->

    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light topbar py-3 mb-4 static-top justify-content-between" style="background-color: #F24B27;">
        <a class="ml-4" style="color:white;font-family:Tahoma; font-weight:1000; font-size:20px" href="/" > MAKANN ENAK </a>  
        @if (!auth()->user())
        <a style="margin-left: 70%; color:black;font-weight: bold;" href="{{url('/login')}}">Login</a>
        <a>  &nbsp &nbsp | &nbsp </a>
        <a class="ml-2" style="color:black;font-weight: bold;" href="{{url('/register')}}">Register</a>
        @endif
        <ul class="navbar-nav ml-auto ">
          @if (auth()->user())
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
              
                <span class="ml-2 d-none d-lg-inline text-solid small" style="color:black;font-weight: bold;">
                  @if (auth()->user()->profile)
                  {{auth()->user()->profile->nama}}
                  @else
                  {{auth()->user()->name}}
                  @endif
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{url('profile')}}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          @endif
        </ul>
        </nav>
        <!-- Topbar -->

        <!-- Container Fluid-->
        <div class="container-fluid mx-5" id="container-wrapper" style="width:95%">
        @yield('content')

          <!-- Modal Logout -->
          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Are you sure you want to logout?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-primary" href="{{ route('logout') }}"
                      onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!---Container Fluid-->
      </div>
      {{-- <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - developed by
              <b><a href="https://indrijunanda.gitlab.io/" target="_blank">indrijunanda</a></b>
            </span>
          </div>
        </div>
      </footer>
      <!-- Footer --> --}}
    </div>
  </div>

  <!-- Scroll to top -->
  <!-- <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a> -->

  <script src="{{asset('ruang-admin/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('ruang-admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('ruang-admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('ruang-admin/js/ruang-admin.min.js')}}"></script>

  @stack('script')

  @include('sweetalert::alert')
</body>

</html>