@extends('layouts.master')

@section('content')
<div class="card-body">
    <form action="/profile" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label for="nama">Nama Lengkap</label>
        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama">
        @error('nama')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan Umur">
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="notelp">No Telepon</label>
        <input type="text" class="form-control" id="notelp" name="notelp" placeholder="Masukkan No Telepon">
        @error('notelp')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Biodata</label> <br>
        <textarea class="form-control" name="bio" id="bio" cols="150" rows="10"></textarea>
        @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="poto">Foto Profile</label>
        <div class="custom-file"> 
          <input type="file" name="poto">
          {{-- <label class="custom-file-label" for="poto">Choose file</label> --}}
          @error('poto')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Buat Profile</button>
    </form>
  </div>

@endsection

