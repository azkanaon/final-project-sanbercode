@extends('layouts.master')

@section('content')
<div class="container">
   <h3>Profile</h3>
    <div class="row">
      <div class="col-3">
        <img src="{{asset('uploads/profile/'.$lastprofile->poto)}}" width="250" class="img-top">
      </div>
      <div class="col-9">
        <p> <b>Nama : </b> {{$lastprofile->nama}}</p>
        <p> <b>Umur : </b> {{$lastprofile->umur}}</p>
        <p> <b>No Telepon : </b> {{$lastprofile->notelp}}</p>
        <p> <b>Biodata : </b> <br> {{$lastprofile->bio}}</p>
        <a href="/profile/{{$lastprofile->id}}/edit" class="btn btn-warning btn-sm mb-3">Edit</a>
      </div>
    </div>
  </div>
@endsection



