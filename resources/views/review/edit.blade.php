@extends('layouts.master')

@section('content')
<h2>Buat Review Makanan</h2>
<div class="card-body">
    <form action="/review/{{$review->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul',$review->judul)}}">
        @error('judul')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="review">Review</label> <br>
        <textarea class="form-control" name="review" id="review" cols="150" rows="10">{{old('review',$review->review )}}</textarea>
        @error('review')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="nilai">Nilai</label>
        <select class="custom-select" name="nilai" id="nilai">
          <option selected disabled>Open this select menu</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        @error('nilai')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="poto">Foto Profile</label>
        <div class="custom-file"> 
          <input type="file" name="poto">
          {{-- <label class="custom-file-label" for="poto">Choose file</label> --}}
          @error('poto')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Edit Review</button>
    </form>
  </div>

  @push('script')
        <script src="https://cdn.tiny.cloud/1/jgkfx7zjjtegf7mgl9go43o100c1imkptnogdl8jkus5kals/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        
        <script>
            tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
        </script>
        @endpush   
@endsection