@extends('layouts.master')

@section('content')
<div class="m-2">
    <div class="container" style="mt-5">
    <div class="d-flex flex-column">
        <button class="btn btn-outline-light mt-2 mb-5">
            <a class="my-5" style="color:#F24B27;font-family:Tahoma; font-weight:1000; font-size:30px; text-align:center; " href="/" > MAKANN ENAK </a>  
        </button>
    </div>
    <a class ="btn btn-primary mb-3" href="/review/create"> Create New Review </a>
    <div class="d-flex flex-column">
        
        <div class="row" style="mt-5">
        @foreach($review as $key => $review)
                <div class="col-md-3"  >
                <div class="card" style="width: 20rem ml-5">
                    <img class="card-img-top" src=" {{asset('img/'.$review->poto)}} " alt="Card image cap" width="100px" height="160px" background-size="cover">
                <div class="card-body">
                    <h4 class="card-title, ml-2"">{{$review->judul}}</h4>
                    <p class="card-text" >{!!$review->review!!}</p>
                    <p class="card-text" >Nilai: {{$review->nilai}}</p>
                </div>
                </div>
                <div style="display :flex;">
                    <a class="btn btn-secondary mr-3 ml-3" href="/review/{{ $review->id }}/edit"> Edit </a> 
                    <form action="/review/{{$review->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
            </div>
                </div>
            @endforeach
    </div>
@endsection