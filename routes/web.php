<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RestoranController@home' );
// Route::get('/', function() {
//     return view('welcome');
// });

Auth::routes();
Route::resource('restoran','RestoranController');
Route::resource('profile','ProfileController');
Route::resource('review','ReviewController');
Route::resource('lokasi','LokasiController');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('lokasi','LokasiController');
    Route::resource('profile','ProfileController');
    Route::resource('review','ReviewController');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user','UserController@index');
Route::get('user/json','UserController@json');
